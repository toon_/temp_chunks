from itertools import islice
from timeit import timeit

a = [1, 2, 3, 4, 5, 6, 7, 8, 9]
cycles = 1_000_000
columns = 2


def print_list0(columns=0, list=[]):
    i, ni, llen = [], [], len(list)
    for n in range(columns):
        i.append(islice(iter(list), n, llen, columns))

    for x in range(llen//columns):
        for n in range(columns):
            ni.append(next(i[n]))
        '(' + (' '.join(('{}',)*columns) + ')').format(*ni)
        ni.clear()


def print_list1(columns=0, list=[]):
    llen = len(list)

    for x in zip(*[islice(iter(list), n, llen, columns) for n in range(columns)]):
        '(' + (' '.join(('{}',)*columns) + ')').format(*x)


def print_list2(columns=0, list=[]):
    i, llen = [], len(list)
    for n in range(columns):
        i.append(islice(iter(list), n, llen, columns))

    for x in zip(*i):
        '(' + (' '.join(('{}',)*columns) + ')').format(*x)


for fn in range(3):
    tm = timeit('print_list{}(columns,a)'.format(fn),
                number=cycles, globals=globals())
    print("print_list{} in {} seconds for {} runs".format(fn, tm, cycles))